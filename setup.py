import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="AntWorker",
    version="0.0.1",
    author="mgrove-wwu, TheClockTwister",
    description="A package for distributed processing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/wiesen.leon/antworker",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
